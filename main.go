package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	InitialMigration()
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})
	r.POST("/user", CreateUser)
	r.GET("/users", GetUsers)
	r.GET("/user/:id", GetUser)
	r.PUT("/user/:id", UpdateUser)
	r.DELETE("/user/:id", DeleteUser)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
