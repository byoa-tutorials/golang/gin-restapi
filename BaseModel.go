package main

import (
	"time"
)

type BaseModel struct {
	ID         uint      `gorm:"primarykey"`
	CreatedBy  string    `gorm:"column:created_by"`
	CreatedOn  time.Time `gorm:"autoCreateTime"`
	ModifiedBy string    `gorm:"column:modified_by"`
	ModifiedOn time.Time `gorm:"autoUpdateTime"`
	Version    uint      `gorm:"default:1"`
	DelFlg     string    `gorm:"default:N"`
}
