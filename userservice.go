package main

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
)

func CreateUser(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")
	user := User{}
	json.NewDecoder(c.Request.Body).Decode(&user)
	DB.Create(&user)
	json.NewEncoder(c.Writer).Encode(&user)

}

func GetUsers(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")
	users := []User{}
	DB.Find(&users)
	json.NewEncoder(c.Writer).Encode(&users)

}
func GetUser(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")
	user := User{}
	result := DB.First(&user, c.Param("id"))
	if result.RowsAffected == 0 {
		json.NewEncoder(c.Writer).Encode("Record not found!")
		return
	}
	json.NewEncoder(c.Writer).Encode(&user)

}

func UpdateUser(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")
	user := User{}
	result := DB.First(&user, c.Param("id"))
	if result.RowsAffected == 0 {
		json.NewEncoder(c.Writer).Encode("Record not found!")
		return
	}
	json.NewDecoder(c.Request.Body).Decode(&user)
	DB.Save(&user)
	json.NewEncoder(c.Writer).Encode(&user)

}

func DeleteUser(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")
	user := User{}
	result := DB.Delete(&user, c.Param("id"))
	if result.RowsAffected == 0 {
		json.NewEncoder(c.Writer).Encode("Record not found!")
		return
	}

	json.NewEncoder(c.Writer).Encode("User is Deleted Successfully")

}
